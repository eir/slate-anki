from typing import (
    Dict,
    Generic,
    Optional,
    TypeVar,
)

T = TypeVar('T')

class TrieNode(Generic[T]):
    def __init__(self):
        self.children: Dict[str, 'TrieNode[T]'] = {}
        self.is_end: bool = False
        self.value: Optional[T] = None

class Trie(Generic[T]):
    def __init__(self):
        self.root: TrieNode[T] = TrieNode[T]()
        self.value: Optional[T] = None

    def insert(self, word: str, value: T):
         node: TrieNode[T] = self.root

         for char in word:
             if char not in node.children:
                 node.children[char] = TrieNode[T]()

             node = node.children[char]

         node.value = value
         node.is_end = True

    def find(self, word: str) -> Optional[T]:
         node: TrieNode[T] = self.root

         for char in word:
             if char not in node.children:
                 return None

             node = node.children[char]

         return node.value

    def find_prefix(self, prefix: str, limit: int | None = None) -> list[str]:
        node = self.root

        for char in prefix:
            if char not in node.children:
                return []

            node = node.children[char]

        return self._collect_words(node, prefix, limit)

    def _collect_words(self, node: TrieNode[T], prefix: str, limit: int | None = None) -> list[str]:
        words = []

        if node.is_end:
            words.append(prefix)

        for char, child in node.children.items():
            if limit is not None and len(words) >= limit:
                break

            words.extend(self._collect_words(child, prefix + char))

        return words

