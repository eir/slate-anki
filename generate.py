import csv

from enum import IntEnum

from dictionary import get_word_suggestions
from number_translation import (
    dig_2_to_card,
    dig_4_to_card_pair,

    dig_1_to_sound_part,
    dig_2_to_sound_part,
    dig_3_to_sound_part,
    dig_4_to_sound_part,
)

class Digits(IntEnum):
    One   = 1
    Two   = 2
    Three = 3
    Four  = 4

str_of_suggestions = lambda s: ', '.join(s) if s else '***'

with open('slate-cards-double.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    # Suits are 0 - 0xF (0 = SpadeSpade, 1 = SpadeDiamond, ..., 3 = SpadeHeart, ..., F = HeartHeart)
    # Ranks are 1 - 0xA9 (1 = AA, 2 = A2, ..., 14 = 2A, ...)
    # Suit is shifted to just left of the ranks and ORed with them
    # (2 Spades, A Hearts) --> 0x300 | 0x0E = 0x30E = 0782 (k-oo-n)

    print('creating card-pair deck...')

    for s in range(4*4):
        for r in range(1, 13*13+1):
            i = (s << 8) | r
            pronunciation = dig_4_to_sound_part(i)

            card_pair = dig_4_to_card_pair(i)
            suggested_words = get_word_suggestions(pronunciation.ipa)

            answer = f'{card_pair}\n\n<details>{i:04}\nipa: {pronunciation.ipa}\nrsp: {pronunciation.respell}</details>'
            writer.writerow([str_of_suggestions(suggested_words), answer])

with open('slate-cards-single.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    # Suits order: Spades, Diamonds, Clubs, Hearts

    print('creating single-card deck...')

    for i in range(13*4):
        i += 1

        pronunciation = dig_2_to_sound_part(i)

        card = dig_2_to_card(i)
        suggested_words = get_word_suggestions(pronunciation.ipa)

        answer = f'{card}\n\n<details>{i:02}\nipa: {pronunciation.ipa}\nrsp: {pronunciation.respell}</details>'
        writer.writerow([str_of_suggestions(suggested_words), answer])


def create_deck_numbers(f: str, num_digits: Digits):
    with open(f, 'w') as csvfile:
        writer = csv.writer(csvfile)
        print(f'creating {f}...')

        d = int(num_digits)

        for i in range(10 ** d):
            match num_digits:
                case Digits.Four:  pronunciation = dig_4_to_sound_part(i)
                case Digits.Three: pronunciation = dig_3_to_sound_part(i)
                case Digits.Two:   pronunciation = dig_2_to_sound_part(i)
                case Digits.One:   pronunciation = dig_1_to_sound_part(i)

            suggested_words = get_word_suggestions(pronunciation.ipa)

            front = f'{str_of_suggestions(suggested_words)}'
            back = f'{i:0{d}}\n\n<details>ipa: {pronunciation.ipa}\nrsp: {pronunciation.respell}</details>'

            writer.writerow([front, back])

create_deck_numbers('slate-digits-4.csv', Digits.Four)
create_deck_numbers('slate-digits-3.csv', Digits.Three)
create_deck_numbers('slate-digits-2.csv', Digits.Two)
create_deck_numbers('slate-digits-1.csv', Digits.One)

print('de-allocating...')
