from enum import Enum, auto

from common import SoundPart

consonants_respell = ['s', 't', 'n', 'm', 'r', 'l', 'j', 'k', 'f', 'p', 'z', 'd', 'h', 'w', 'th', 'sh', 'ch', 'g', 'v', 'b', 'y', 'dh', 'bl', 'br', 'dr', 'dw', 'fl', 'fr', 'gl', 'gr', 'kl', 'kr', 'kw', 'pl', 'pr', 'shr', 'sk', 'skr', 'skw', 'sl', 'sm', 'sn', 'sp', 'spr', 'st', 'str', 'sw', 'thr', 'tr', 'tw']

consonants_ipa = ['s', 't', 'n', 'm', 'r', 'l', 'd͡ʒ', 'k', 'f', 'p', 'z', 'd', 'h', 'w', 'θ', 'ʃ', 'tʃ', 'ɡ', 'v', 'b', 'j', 'ð', 'bl', 'br', 'dr', 'dw', 'fl', 'fr', 'ɡl', 'ɡr', 'kl', 'kr', 'kw', 'pl', 'pr', 'ʃr', 'sk', 'skr', 'skw', 'sl', 'sm', 'sn', 'sp', 'spr', 'st', 'str', 'sw', 'θr', 'tr', 'tw']

vowels_respell = ['ah', 'ih', 'uh', 'eh', 'a', 'oh', 'ay', 'eye', 'oo', 'ee']
vowels_ipa = ['ɑː', 'ɪ', 'ʌ', 'ɛ', 'æ', 'oʊ', 'eɪ', 'aɪ', 'u(ː)', 'i(ː)']

blends_final_respell = ['ft', 'ks', 'kt', 'lb', 'ld', 'lf', 'lk', 'lm', 'ls', 'lt', 'mp', 'ms', 'nch', 'nd', 'nj', 'nk', 'ns', 'nt', 'nth', 'pt', 'rch', 'rd', 'rf', 'rj', 'rk', 'rm', 'rn', 'rs', 'rt', 'rth']

blends_final_ipa = ['ft', 'ks', 'kt', 'lb', 'ld', 'lf', 'lk', 'lm', 'ls', 'lt', 'mp', 'ms', 'ntʃ', 'nd', 'nd͡ʒ', 'nk', 'ns', 'nt', 'nθ', 'pt', 'rtʃ', 'rd', 'rf', 'rd͡ʒ', 'rk', 'rm', 'rn', 'rs', 'rt', 'rθ']

finals_other_respell = ['bz', 'dz', 'fs', 'gz', 'kst', 'lch', 'lj', 'lp', 'lv', 'lz', 'mf', 'mz', 'ng', 'zh', 'nz', 'ps', 'rz', 'ths', 'ts', 'vz']
finals_other_ipa = ['bz', 'dz', 'fs', 'ɡz', 'kst', 'ltʃ', 'ld͡ʒ', 'lp', 'lv', 'lz', 'mf', 'mz', 'ŋ', 'ʒ', 'nz', 'ps', 'rz', 'θs', 'ts', 'vz']

def apply_special_ending_cons(s: SoundPart) -> SoundPart:
    # 2/50 consonants are changed if ending
    rsp = s.respell
    ipa = s.ipa

    if 'h' == rsp:
        rsp = 'ng'
        ipa = 'ŋ'

    elif 'w' == rsp:
        rsp = 'zh'
        ipa = 'ʒ'

    return SoundPart(respell=rsp, ipa=ipa)

def apply_r_rules(s: SoundPart) -> SoundPart:
    # r-endings (4) require some special handling sometimes
    rsp = s.respell
    ipa = s.ipa

    if 'ih-r' in rsp:
        rsp = rsp.replace('ih-r', 'oy')
        ipa = ipa.replace('ɪ-r', 'ɔɪ')

    elif 'uh-r' in rsp:
        rsp = rsp.replace('uh-r', 'uu')
        ipa = ipa.replace('ʌ-r', 'ʊ')

    elif 'a-r' in rsp:
        rsp = rsp.replace('a-r', 'ow')
        ipa = ipa.replace('æ-r', 'aʊ')

    return SoundPart(respell=rsp, ipa=ipa)

def get_ending_dig4(i: int) -> SoundPart:
    index_ones = i % 10
    index_tens = (i // 10) % 10

    if i >= 5000:
        index_ones += 10

    t = SoundPart(
        respell = vowels_respell[index_tens],
        ipa = vowels_ipa[index_tens],
    )

    e = SoundPart(
        respell = consonants_respell[index_ones],
        ipa = consonants_ipa[index_ones],
    )

    res = t + apply_special_ending_cons(e)

    return apply_r_rules(res)

def dig_4_to_sound_part(i: int) -> SoundPart:
    assert i >= 0
    assert i < 10000

    i_consonants = i // 100 % 50

    prefix = SoundPart(
        respell = consonants_respell[i_consonants],
        ipa = consonants_ipa[i_consonants],
    )

    ending = get_ending_dig4(i)

    return prefix + ending

suits = ['♠️', '♦️', '♣️', '♥️']
ranks = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']

def dig_4_to_card_pair(i: int) -> str:
    assert i > 0
    assert i <= 0xF00 | 13*13

    index_suits = (i & 0xF00) >> 8
    index_ranks = (i & 0xFF) - 1

    rank_first = ranks[index_ranks // 13]
    rank_second = ranks[index_ranks % 13]

    suit_first = suits[index_suits >> 2]
    suit_second = suits[index_suits & 0x3]

    return rank_first + suit_first + ' ' + rank_second + suit_second

def dig_2_to_card(i: int) -> str:
    assert i > 0
    assert i <= 13*4

    i -= 1

    suit = suits[i // 13]
    rank = ranks[i % 13]

    return f'{rank}{suit}'

def dig_3_to_sound_part(i: int) -> SoundPart:
    assert i >= 0
    assert i < 1000

    i_consonants = i // 10 % 50
    i_final_blends = i // 10 - 70
    i_vowels = i % 10

    consonant = SoundPart(
        respell = consonants_respell[i_consonants],
        ipa = consonants_ipa[i_consonants],
    )

    vowel = SoundPart(
        respell = vowels_respell[i_vowels],
        ipa = vowels_ipa[i_vowels],
    )

    if i < 500:
        return consonant + vowel

    if i >= 620 and i < 640:
        consonant = apply_special_ending_cons(consonant)

    res = vowel + consonant

    if i >= 700:
        final_blend = SoundPart(
            respell = blends_final_respell[i_final_blends],
            ipa = blends_final_ipa[i_final_blends],
        )

        res = vowel + final_blend

    return apply_r_rules(res)

def dig_2_to_sound_part(i: int) -> SoundPart:
    assert i >= 0
    assert i < 100

    if i < 50:
        rsp = consonants_respell[i]
        ipa = consonants_ipa[i]
    elif i < 70:
        rsp = finals_other_respell[i - 50]
        ipa = finals_other_ipa[i - 50]
    else:
        rsp = blends_final_respell[i - 70]
        ipa = blends_final_ipa[i - 70]

    return SoundPart(respell=rsp, ipa=ipa)

def dig_1_to_sound_part(i: int) -> SoundPart:
    assert i >= 0
    assert i < 10

    r_ending = SoundPart(respell='r', ipa='r')

    rsp = vowels_respell[i]
    ipa = vowels_ipa[i]

    res = SoundPart(respell=rsp, ipa=ipa) + r_ending

    return apply_r_rules(res)
