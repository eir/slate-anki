from json import loads

from common import WordInfo, progress_update
from trie import Trie

ipa_lookup_en = Trie[WordInfo]()
ipa_lookup_non_en = Trie[WordInfo]()

def get_ipa_variations(s: str) -> list[str]:
    # To help with cases like 'abhorrance' : 'əbˈhɒɹ.n̩(t)s'
    # əbˈhɒɹ.n̩ts and əbˈhɒɹ.n̩s are both valid
    if '(' not in s or ')' not in s:
        return [s]

    # WARN: does not support nested parens
    index_open = s.index('(')
    index_close = s.index(')')

    group = s[index_open+1:index_close]
    with_group = s[:index_open] + group + s[index_close+1:]
    without_group = s[:index_open] + s[index_close+1:]

    return get_ipa_variations(with_group) + get_ipa_variations(without_group)


def build_ipa_lookup():
    def build(file: str, root: Trie[WordInfo], index_max: int):
        with open(file, 'r') as f:
            index = 0

            for line in f:
                index += 1

                obj = loads(line)
                ipas = obj['ipa']

                if 0 == index & 0xFF or index_max == index:
                    progress_update(f'Loading {file}...', index_max, index)

                for ipa in ipas:
                    ''' # TODO: Make this an option
                    if '[' in ipa:
                        # Skip rare pronunciations
                        continue
                    '''

                    for char in ['/', 'ˈ', 'ˌ', '.', '[', ']']:
                        ipa = ipa.replace(char, '')

                    for vr in get_ipa_variations(ipa):
                        if root.find(vr) is not None:
                            continue

                        root.insert(vr, obj)

    build('en.json', ipa_lookup_en, 110631)
    print()
    build('non-en.json', ipa_lookup_non_en, 2384090)
    print('\ndone building word lookup table')

build_ipa_lookup()

MAX_SUGGESTIONS = 12

def get_similar_sounding(s: str) -> list[str]:
    similar_pairs = [
        ('æ', 'ə'),
        ('æ', 'ɛə'),
        ('æ', 'ɛ'),

        ('ɛ', 'æ'),
        ('ɛ', 'ɛə'),
        ('ɛ', 'ɜː'),

        ('ɔ', 'ɒ'),

        ('oʊ', 'ɒ'),
        ('oʊ', 'ɔː'),

        ('ʌ', 'ʊ'),
        ('ʊ', 'ʌ'),

        ('r', 'ɹ'),
        ('ɹ', 'r'),
    ]

    res = []

    for a, b in similar_pairs:
        if a in s:
            res.extend(get_ipa_variations(s.replace(a, b)))

    return res


def get_word_suggestions(s: str) -> list[str]:
    sc = s.replace('-', '')

    vr = get_ipa_variations(sc) + get_similar_sounding(sc)
    all_matches = []

    def add_suggestions(t: Trie[WordInfo], l: int, to_str, prefix: str):
        # Find shortest l matches
        matching = t.find_prefix(prefix, limit=l)
        matching.sort(key=len)

        for ipa in matching[:l]:
            s = to_str(ipa)

            if s not in all_matches:
                all_matches.append(s)

    def str_of_en(ipa: str) -> str:
        return ipa_lookup_en.find(ipa)['word']

    def str_of_non_en(ipa: str) -> str:
        info = ipa_lookup_non_en.find(ipa)
        return f'{info["word"]} ({info["lang_code"]})'

    for v in vr:
        if len(all_matches) >= MAX_SUGGESTIONS:
            break

        add_suggestions(ipa_lookup_en, MAX_SUGGESTIONS, str_of_en, v)

        max_non_en_matches = max(3, MAX_SUGGESTIONS - len(all_matches))
        add_suggestions(ipa_lookup_non_en, max_non_en_matches, str_of_non_en, v)

    return all_matches
