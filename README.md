### Pre-generated Decks: https://app.box.com/s/0ks64he4ck5bzjjjk128a1z9wbkg3ykb
### Importing to Anki:
 - File --> Import
 - Separator: Comma
 - Card Type: Basic (with reversed card)


### Generating Decks Yourself:
**Python Version**: 3.10+
Be sure to unzip `dicts.zip`
```python
python generate.py
```
**Warning:**`non-en.json` is large, so you would need ~6GB RAM.

##### Data
Source: wiktionary via https://kaikki.org/dictionary/rawdata.html (raw Wiktextract data)

```bash
cat raw-wiktextract-data.json | jq 'if "en" == .lang_code and .word and .sounds and .sounds[0].ipa then { word: .word, ipa: [.sounds[].ipa // empty], lang_code: .lang_code, gloss: .senses[0].glosses[0] } else empty end' -c > en.json

cat raw-wiktextract-data.json | jq 'if "en" != .lang_code and .word and .sounds and .sounds[0].ipa then { word: .word, ipa: [.sounds[].ipa // empty], lang_code: .lang_code, gloss: .senses[0].glosses[0] } else empty end' -c > non-en.json
```
