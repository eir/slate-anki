from dataclasses import dataclass
from sys import stdout
from typing import TypedDict

def progress_update(title: str, index_max: int, index: int):
    BAR_SIZE = 66

    percent_complete = index / index_max
    percent_display = int(percent_complete * 100)
    filled_count = min(BAR_SIZE, int(percent_complete * BAR_SIZE))

    print(f'{title} [{"*" * filled_count}{" " * (BAR_SIZE-filled_count)}] {percent_display}%', end='\r', file=stdout, flush=True)

@dataclass
class SoundPart:
    respell: str
    ipa: str

    def __add__(self, other: 'SoundPart') -> 'SoundPart':
        return SoundPart(
            respell = self.respell + '-' + other.respell,
            ipa = self.ipa + '-' + other.ipa,
        )

class WordInfo(TypedDict):
    word: str
    ipa: list[str]
    lang_code: str
    gloss: str
